# Level Design Porn

Brainstorming-cards related to **Level Design**.

- arrange & think & discuss!
- read between the cards!

[`leveldesignporn.csongorb.com`](http://leveldesignporn.csongorb.com)

## Version-History

### Release 2.3 (26. March 2021)

- some new / heavily updated cards
	- metrics
	- space
	- moving perspective
	- process
	- puzzle design
	- cognitive modeling
	- visual perception
	- worldbuilding
	- speedrunning
	- non-euclidean geometry
	- spreepark berlin 
- improvements
	- new / fixed links
	- some new PDFs

### Release 2.2 (20. March 2018)

- updated to Porn Creator 0.3
	- added rows

### Release 2.1 (11. May 2017)

- improvements
	- new / fixed links

### Release 2.0 (16. April 2017)

- updated to Porn Creator 0.22
- complete redesign of the card set
	- new card types
	- lot of new cards & links

### Release 1.0 (May 2014)

- first release
	- a combination of
		- Wordpress blog (theme: Evolutive2)
		- lot of html-files (the links)
		- printable PDF (incl. QR-codes)
- primarily made for a course at MDH Munich
	- was several times reused for other courses
- kept here for documentation purposes
	- can't be used without the other parts (Wordpress, etc.)
