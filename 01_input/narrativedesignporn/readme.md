# Narrative Design Porn

Brainstorming-cards related to **Narrative Design**.

- arrange & think & discuss!
- read between the cards!

[`narrativedesignporn.csongorb.com`](http://narrativedesignporn.csongorb.com)

## Version-History

### Todos / Ideas / Bugs

Ideas for New Cards / Topics:

- ???

### Release 1.6 (26. March 2021) - The Worldbuilding Update

- links overhaul
	- hopefully all broken links repaired
	- lot of new links added
- new cards
	- quest design
	- fiction or documentary
	- atmosphere
	- worldbuilding
	- writers room
	- simulation
	- myth building
	- space
	- utopia
	- thimbleweed park
	- duckburg
	- a short hike
	- small worlds
	- wimmlingen
	- any transmedia project
- added a lot of new PDFs 

### Release 1.5 (8. March 2020)

- links overhaul
	- hopefully all broken links repaired
	- lot of new links added
- new cards
	- Semiotics
	- Roguelikes
	- Walking Simulators

### Release 1.4 (20. March 2018)

- updated to Porn Creator 0.3
	- added rows
- new cards
	- Adaptation
	- Gating
	- Uncharted 4
	- Humans as Systems

### Release 1.3 (11. May 2017)

- improvements
	- new / fixed links

### Release 1.2 (16. April 2017)

- minor redesign
	- adjusting to Porn Creator 0.22

### Release 1.1 (April 2017)

New Cards:

- Citizen Kane
- Any Game
- Triggers & Events
- Authorship
- Stories Told by Games

Misc:

- changed "Sujet" into "Sujet & Diversity"
- changed "Drama Machine" into "Drama Manager"
- changed "Message Model" into "Game Aesthetics"
- lot of typos
- lot of improved questions
- some reworked links

### Release 1.0 (April 2016)

- first release
- primarily made for a course at the BTK for summer semester 2016
