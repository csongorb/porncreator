
// ========================================
// Card class
// ========================================

class Card {
  
  // yes, I'm too lazy to use inharitance...
  // https://processing.org/examples/inheritance.html

  // general
  int cardCategoryId;
  String type;
  String text;

  // type "link"
  String originalTitle;
  String[] title;
  String[] links;
  String filename;
  PImage qrCode;

  // type "display"
  int[] tags;

  Card() {
  }

  void getFilename() {
    filename = removeSpecChars(title[0].toLowerCase());
  }
}

// ========================================
// Card-Category class
// ========================================

class CardCategory {
  String title;
  String c;
  int row;
  boolean onlyBorder;

  CardCategory(String _title, String _c, int _row, boolean _onlyBorder) {
    title = _title;
    c = _c;
    row = _row;
    onlyBorder = _onlyBorder;
  }
}