
void copyTextFiles() {

  String[] fileNames;
  String sourceFolder;
  String destFolder;

  // copy text-PDFs

  sourceFolder = myProjectPath + myInputFolder + selectedProjectFolder + "/texts";
  destFolder = myProjectPath + myOutputFolder + selectedProjectFolder + "/texts";

  File libraryFolder = new File (destFolder);
  libraryFolder.mkdir();

  fileNames = listFileNames(sourceFolder);
  for (int i = 0; i < fileNames.length; i++) {
    copyFile(sourceFolder + "/" + fileNames[i], destFolder + "/" + fileNames[i]);
  }
}

void copyPornFiles() {

  String[] fileNames;
  String sourceFolder;
  String destFolder;

  // copy p5js

  sourceFolder = myProjectPath + myPorncreatorFolder + "/p5js";
  destFolder = myProjectPath + myOutputFolder + selectedProjectFolder + "/libraries";

  File libraryFolder = new File (destFolder);
  libraryFolder.mkdir();

  fileNames = listFileNames(sourceFolder);
  for (int i = 0; i < fileNames.length; i++) {
    copyFile(sourceFolder + "/" + fileNames[i], destFolder + "/" + fileNames[i]);
  }

  // copy index.html & style.css & create scripts folder

  sourceFolder = myProjectPath + myPorncreatorFolder + "/pornfragments";
  destFolder = myProjectPath + myOutputFolder + selectedProjectFolder;

  fileNames = listFileNames(sourceFolder);
  for (int i = 0; i < fileNames.length; i++) {
    copyFile(sourceFolder + "/" + fileNames[i], destFolder + "/" + fileNames[i]);
  }

  // copy scripts

  sourceFolder = myProjectPath + myPorncreatorFolder + "/pornfragments/scripts";
  destFolder = myProjectPath + myOutputFolder + selectedProjectFolder + "/scripts";

  File scriptFolder = new File (destFolder);
  scriptFolder.mkdir();

  fileNames = listFileNames(sourceFolder);

  for (int i = 0; i < fileNames.length; i++) {
    copyFile(sourceFolder + "/" + fileNames[i], destFolder + "/" + fileNames[i]);
  }
}

void copyFile(String sourceStr, String destStr) {

  Path source = Paths.get(sourceStr);
  Path dest = Paths.get(destStr);

  try 
  {
    Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
  } 
  catch(IOException e) 
  {
    e.printStackTrace();
  }
}

// This function returns all the files in a directory as an array of Strings  
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}
// https://processing.org/examples/directorylist.html