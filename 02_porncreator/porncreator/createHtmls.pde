
// ========================================
// create Link HTML Files
// ========================================

void createHtmls() {

  for (int i = 0; i < cards.length; i++) {

    if (cards[i].type.equals("link")) {

      cardHtmls = createWriter(myProjectPath + myOutputFolder + selectedProjectFolder + "/" + cards[i].filename + ".html"); 
      cardHtmls.println("<html>");

      // ===============
      // head

      cardHtmls.println();

      writeStyle(cardHtmls, cardCategories[cards[i].cardCategoryId].c);
      cardHtmls.println();

      cardHtmls.println("    <script language=javascript>");
      cardHtmls.println();
      cardHtmls.println("      var i;");
      cardHtmls.println("      var arLinks = new Array();");
      cardHtmls.println();
      for (int ii = 0; ii < cards[i].links.length; ii++) {
        cardHtmls.println("      arLinks[" + ii + "] = '" + cards[i].links[ii] + "';");
      }

      cardHtmls.println();
      cardHtmls.println("      var openRnd = function() {");
      cardHtmls.println("        var randIdx = Math.random() * arLinks.length;");
      cardHtmls.println("        randIdx = parseInt(randIdx, 10);");
      cardHtmls.println("        var link = arLinks[randIdx];");
      cardHtmls.println("        window.open(link,'_blank');");
      cardHtmls.println("        }");
      cardHtmls.println();

      cardHtmls.println("    </script>");

      cardHtmls.println("  <head>");
      cardHtmls.println("  </head>");
      cardHtmls.println();

      // ===============
      // body

      String tmp_title = "";
      for (int ii = 0; ii < cards[i].title.length; ii++) {
        tmp_title = tmp_title + cards[i].title[ii] + " ";
      }

      cardHtmls.println("  <body>");
      cardHtmls.println();

      cardHtmls.println("    <div class='container'>");
      cardHtmls.println("      <span class='title'>" + tmp_title + "</span><br>");
      cardHtmls.println("      <span class='text'>" + cards[i].text + "</span><br>");
      cardHtmls.println("      </br>");

      for (int ii = 0; ii < cards[i].links.length; ii++) {
        cardHtmls.println("      <span class='text'>(<a target='_blank' href='" + cards[i].links[ii] + "'>" + (ii+1) + "</a>)</span>");
      }
      cardHtmls.println("      <span class='text'>(<a target='_blank' href='#' onclick='openRnd();'>rnd</a>)</span>");

      cardHtmls.println("    </div>");
      cardHtmls.println();

      cardHtmls.println("  </body>");
      cardHtmls.println("</html>");

      cardHtmls.flush(); // Writes the remaining data to the file
      cardHtmls.close(); // Finishes the file
    }
  }

  // ================================
  // create PDFs.html
  // ================================

  PDFhtml = createWriter(myProjectPath + myOutputFolder + selectedProjectFolder + "/pdfs.html"); 
  PDFhtml.println("<html>");

  // ===============
  // head

  PDFhtml.println("  <head>");
  PDFhtml.println();

  writeStyle(PDFhtml, "000000");
  PDFhtml.println();

  PDFhtml.println("  <head>");
  PDFhtml.println("  </head>");
  PDFhtml.println();

  // ===============
  // body

  PDFhtml.println("  <body>");
  PDFhtml.println();

  PDFhtml.println("    <div class='container'>");

  PDFhtml.println("      <div class='text'>");
  PDFhtml.println("        <a target='_blank' href='pdfs/cardsA4.pdf'>/cardsA4.pdf</a><br>");
  PDFhtml.println("        <span>print on colored paper</span><br>");
  PDFhtml.println("      </div>");
  PDFhtml.println("      <br>");
  PDFhtml.println("      <hr>");
  PDFhtml.println("      <br>");
  PDFhtml.println("      <div class='text'>");
  PDFhtml.println("        <a target='_blank' href='pdfs/singleCards.pdf'>/singleCards.pdf</a><br>");
  PDFhtml.println("        <span>print with card printer service</span><br>");
  PDFhtml.println("      </div>");

  PDFhtml.println("    </div>");
  PDFhtml.println();

  PDFhtml.println("  </body>");
  PDFhtml.println("</html>");

  PDFhtml.flush(); // Writes the remaining data to the file
  PDFhtml.close(); // Finishes the file
}

void writeStyle(PrintWriter myWriter, String bgColor) {

  myWriter.println("    <style>");
  myWriter.println("      body {");
  myWriter.println("        font-family: 'Helvetica Neue';");
  myWriter.println("        font-weight: lighter;");
  myWriter.println("        color: #FFFFFF;");
  myWriter.println("        text-align: center;");
  myWriter.println("        background-color: #" + bgColor + ";");
  myWriter.println("      }");
  myWriter.println("      .title {");
  myWriter.println("        font-size: 200%;");
  myWriter.println("      }");
  myWriter.println("      .text {");
  myWriter.println("        text-align: center;");
  myWriter.println("        font-size: 150%;");
  myWriter.println("      }");
  myWriter.println("      .container {");
  myWriter.println("        position: absolute;");
  myWriter.println("        top: 50%;");
  myWriter.println("        left: 50%;");
  myWriter.println("        -moz-transform: translateX(-50%) translateY(-50%);");
  myWriter.println("        -webkit-transform: translateX(-50%) translateY(-50%);");
  myWriter.println("        transform: translateX(-50%) translateY(-50%);");
  myWriter.println("      }");
  myWriter.println("      a {");
  myWriter.println("        color: #FFFFFF;");
  myWriter.println("      }");
  myWriter.println("    </style>");
}
