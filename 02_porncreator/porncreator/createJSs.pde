
// ========================================
// create Card.js for PornViewer
// ========================================

void createCardsJs() {    //SUGGESTION: An alternative saving format would allow you to directly create a "link only" version of the site while still being able to load the cards. This translation seems like it is mainly a cause for trouble.
  
  //SUGGESTION: You can write quotation marks (") in a string by "escaping" it with "\" like this: "\"". Alternativly saving it in a variable might be better then "char(34)". String.format might help with making this more readable
  
  
  
  cardsJs = createWriter(myProjectPath + myOutputFolder + selectedProjectFolder + "/cards/cards.js"); 

  cardsJs.println("function loadCards(){");
  cardsJs.println();

  for (int i = 0; i < cards.length; i++) {
    cardsJs.print("    ");
    cardsJs.print("cards[" + i + "] = new Card(" + char(34) + cards[i].type + char(34) + ", ");
    //with string.format and quotation marks the previous line becomes:
  //cardsJs.print(String.format(  "cards[%s] = new Card( \" %s \", "  , i, cards[i].type));

    cardsJs.print(cards[i].cardCategoryId + ", ");
    cardsJs.print(char(34) + cards[i].originalTitle + char(34) + ", ");
    cardsJs.print(char(34) + cards[i].filename + char(34) + ", ");
    cardsJs.print(char(34) + cards[i].text + char(34) + ", ");
    cardsJs.print(char(34));
    for (int i2 = 0; i2 < cards[i].tags.length; i2++) {
      cardsJs.print(cards[i].tags[i2]);
      if (i2 != cards[i].tags.length - 1){
        cardsJs.print(",");
      }
    }
    cardsJs.print(char(34));
    cardsJs.print(");");
    cardsJs.println();
  }

  cardsJs.println();
  cardsJs.println("}");

  cardsJs.flush(); // Writes the remaining data to the file
  cardsJs.close(); // Finishes the file
}

// ========================================
// create CardCategories.js for PornViewer
// ========================================

void createCardCategoriesJs() {

  cardCategoriesJs = createWriter(myProjectPath + myOutputFolder + selectedProjectFolder + "/cards/cardCategories.js"); 

  cardCategoriesJs.println("function loadCardCategories(){");
  cardCategoriesJs.println();

  for (int i = 0; i < cardCategories.length; i++) {
    cardCategoriesJs.print("    ");
    cardCategoriesJs.print("cardCategories[" + i + "] = new cardCategory(" + char(34));
    cardCategoriesJs.print(cardCategories[i].title + char(34) + ", ");
    cardCategoriesJs.print("color(" + char(34) + "#" + cardCategories[i].c + char(34) + "), ");
    cardCategoriesJs.print(str(cardCategories[i].onlyBorder) + ", ");
    cardCategoriesJs.print(cardCategories[i].row);
    cardCategoriesJs.print(");");
    cardCategoriesJs.println();
  }

  cardCategoriesJs.println();
  cardCategoriesJs.println("}");

  cardCategoriesJs.flush(); // Writes the remaining data to the file
  cardCategoriesJs.close(); // Finishes the file
}
