

//base card class, used to visualize the cards
class Card {

    constructor(_cType, _cID, _cTitle, _filename, _cText, _cTagsAsString) {

        this.cType = _cType;
        this.cID = _cID;
        this.cTitle = _cTitle;
        this.filename = _filename;
        this.cText = _cText;

        this.cTags = [];
        if (_cTagsAsString != "") {
            var splitString = split(_cTagsAsString, ",");
            for (var i = 0; i < splitString.length; i++) {
                this.cTags[i] = int(splitString[i]);
            }
        } else {
            this.cTags[0] = 0;
        }

        this.cColor = cardCategories[this.cID].tColor;
        this.onlyBorder = cardCategories[this.cID].onlyBorder;
        this.cRow = cardCategories[this.cID].tRow;

        this.cWidth = 160;
        this.cHeight = 160;

        this.textBorder = 10;
        this.textSize = 14;
        this.linkSize = 10;
        this.linkText = "give me porn!"
        this.linkOffsetFactor = 5;
        this.cLink = this.filename + ".html"
        this.visible = true;
        this.bColor = color(50);
        this.shuffleSpiel = 5;
        this.rotationSpiel = 0.05;
        this.mOverAndTopCard = false;

        this.xPos = 0;
        this.yPos = 0;
        this.rotation = 0;

        this.xVel = 0;
        this.yVel = 0;


    }

    //returns true if the mouse is over this card
    mouseOver() {
        var mOver = false;
        if (mouseX > this.xPos - (this.cWidth / 2) && mouseX < this.xPos + (this.cWidth / 2)) {
            if (mouseY > this.yPos - (this.cHeight / 2) && mouseY < this.yPos + (this.cHeight / 2)) {
                mOver = true;
            }
        }
        return mOver;
    }

    //returns true if the mouse is over this cards link
    mouseOverLink() {
        var mOver = false;
        if (mouseX > this.xPos - textWidth(this.linkText) / 2 && mouseX < this.xPos + textWidth(this.linkText) / 2) {
            if (mouseY > this.yPos + this.cHeight / this.linkOffsetFactor - this.linkSize / 2 && mouseY < this.yPos + this.cHeight / this.linkOffsetFactor + this.linkSize / 2) {
                mOver = true;
            }
        }
        return mOver;
    }

     //moves the card by a small amount randomly
    newPos(_xp, _yp) {
        this.xPos = _xp + random(-this.shuffleSpiel, this.shuffleSpiel);
        this.yPos = _yp + random(-this.shuffleSpiel, this.shuffleSpiel);
    }

    //rotates the card by a small amount randomly
    newRotation() {
        this.rotation = random(-this.rotationSpiel, this.rotationSpiel);
    }

    //called by sketch on "draw" visualizes the card
    display() {

        if (this.visible) {

            push();
            translate(this.xPos, this.yPos);
            rotate(this.rotation);

            textAlign(CENTER, CENTER);
            strokeWeight(1);

            if (this.onlyBorder) {
                stroke(this.cColor);
                fill(0);
            } else {
                stroke(this.bColor);
                fill(this.cColor);
            }
            rect(0, 0, this.cWidth, this.cHeight, 5);

            if (this.cType == "link") {

                noStroke();
                fill(200);
                strokeWeight(0);
                textSize(this.textSize);
                var splitTitle = split(this.cTitle, "/");
                for (var i = 0; i < splitTitle.length; i++) {
                    text(splitTitle[i], 0, -20 + (i * 16), this.cWidth - (this.textBorder * 2), this.cHeight - (this.textBorder * 2));
                }

                if (this.mOverAndTopCard) {
                    textSize(this.linkSize);
                    noStroke();
                    //fill(0);
                    if (this.mouseOverLink()) {
                        cursor(HAND);
                        textStyle(ITALIC);
                    } else {
                        textStyle(NORMAL);
                    }
                    text(this.linkText, 0, 0 + this.cHeight / this.linkOffsetFactor, this.cWidth, this.cHeight);
                }
            }

            if (this.cType == "display") {

                // text
                noStroke();
                fill(200);
                strokeWeight(0);
                textSize(this.textSize);
                text(this.cText, 0, -this.cHeight / 2 / 10 * 2, this.cWidth - (this.textBorder * 2), this.cHeight - (this.textBorder * 2));

                // tags
                ellipseMode(CENTER);
                var gap = 20;
                var r = 10;
                var tagWidth = gap * (this.cTags.length - 1);
                for (var i = 0; i < this.cTags.length; i++) {
                    fill(cardCategories[this.cTags[i]].tColor);
                    ellipse(-(tagWidth / 2) + (i * gap), this.cHeight / 2 / 10 * 7, r, r)
                }
            }

            pop();
        }
    }

    //called by sketch on "draw" updates the cards physics
    update() {
        if (Math.abs(this.xVel) > 0.05) {
            this.xPos += this.xVel;
            this.xVel *= dragMult;
        }

        if (Math.abs(this.yVel) > 0.05) {
            this.yPos += this.yVel;
            this.yVel *= dragMult;
        }

        //bouncy walls

        if (this.xPos < this.cWidth/2) {
            this.xPos = this.cWidth / 2;
            this.xVel = Math.abs(this.xVel); //force positive
        }

        if (this.xPos > windowWidth - this.cWidth/2) {
            this.xPos = windowWidth - this.cWidth / 2;
            this.xVel = -Math.abs(this.xVel); //force negative
        }

        if (this.yPos < this.cHeight/2) {
            this.yPos = this.cHeight / 2;
            this.yVel = Math.abs(this.yVel); //force positive
        }

        if (this.yPos > windowHeight - this.cHeight/2) {
            this.yPos = windowHeight - this.cHeight / 2;
            this.yVel = -Math.abs(this.yVel); //force negative
        }

    }

    setMomentum(xM, yM) {
        this.xVel = xM;
        this.yVel = yM;
    }
}


function cardCategory(_tTitle, _tColor, _onlyBorder, _tRow) {
    this.tTitle = _tTitle;
    this.tColor = _tColor;
    this.tRow = _tRow;
    this.onlyBorder = _onlyBorder;
    this.points = [];
}

function backLine(_x, _y, _c) {
    this.points = [];
    this.points[0] = createVector(_x, _y);
    this.c = _c;

    this.newSegment = function (_xp, _yp) {
        this.points[this.points.length] = createVector(_xp, _yp);
    }

    this.draw = function () {
        stroke(this.c);
        noFill();
        strokeWeight(3);
        beginShape();
        for (var i = 0; i < this.points.length; i++) {
            curveVertex(this.points[i].x, this.points[i].y);
        }
        endShape();
    }
}
