# Porn Creator

- a **tool** to create **brainstorming cards**
	- consisting of:
		- **digital cards**
		- **print & play PDFs**
		- a lot of **links**
	- **arrange / think / discuss!**
- a **premade card sets** around a game design related topics
	- **Dissecting Games**
		- the raw material & credits: [`/01_input/dissectinggames/`](/01_input/dissectinggames/)
		- the finished set: [`dissectinggames.csongorb.com`](http://dissectinggames.csongorb.com)
	- **Narrative Design Porn**
		- the raw material: [`/01_input/narrativedesignporn/`](/01_input/narrativedesignporn/)
		- the finished set: [`narrativedesignporn.csongorb.com`](http://narrativedesignporn.csongorb.com)
	- **Level Design Porn**
		- the raw material: [`/01_input/leveldesignporn/`](/01_input/leveldesignporn/)
		- the finished set: [`leveldesignporn.csongorb.com`](http://leveldesignporn.csongorb.com)
	- **Interface Design Cards**
		- the raw material: [`/01_input/interfacedesigncards/`](/01_input/interfacedesigncards/)
		- the finished set: [`interfacedesigncards.csongorb.com`](http://interfacedesigncards.csongorb.com)
- **make your own set!**

## Structure

- [`/01_input/`](/01_input/)
	- the **raw material** (XML, etc.) of the card sets, in folders
- [`/02_porncreator/`](/02_porncreator/)
	- a **[Processing](https://processing.org/) sketch** creates...
	- ...out of the **raw material**...
	- ...with the help of...
		- ...some **PornFragments**...
		- ...and **p5.js libraries**...
	- ...the required files of a **complete p5.js project**
- [`/03_builds/`](/03_builds/)
	- the finished **[p5.js](http://p5js.org/) projects**

## Log / Version-History

### Todos / Bugs / Ideas

PornCreator:

- other
	- add 1 html-page with ALL links
- nicely commented code?
- improved architecture?
	- better streamlined card objects (rethink types, etc.)
- more design possibilities for each card set
	- background color? font size? font color?
	- individual "give me porn!" - text
- PDF generator
	- meinspiel.de cards
		- create back-image (different color for each row)
		- create empty-pages
	- more information on how to print the PDFs
	- bug: A4-PDF has the wrong size!
	- front page with informations (and title card)
- PDF-generation code is a mess!
- "give me porn!" stays also without mouseOver (bug)
- speed improvements! why is all this sooo slow?
- more mobile friendly? how with p5.js?
- add template card set

### Release 0.32 (Work in Progress)

- p5.js updated to v1.3.0

### Release 0.31 (8. March 2020)

- bugfixes
	- link-opening-randomization is working again
- p5.js updated to v1.0.0
- some contributions from Luca Andre Martinelli
	- commented processing files a bit, added suggestions
	- commented sketch.js and objects.js methods
	- gave the cards momentum when released
	- added bouncy walls

### Release 0.3 (February 2018)

- added
	- display cards in rows (incl. menu)
		- stacked cards in centered stacks
	- new card type: display
		- tags (incl. sorting)
		- onlyBorder
	- tags visible online and on the PDFs
	- debug mode: framerate
	- PDFs sup-page
	- each card links to 1 html-page
		- all links as list
	- menu: show/dont-show a whole row
- removed
	- question html-pages
- improvements
	- clearer variable names
		- cardCategories, etc.
	- nicer drawing algorithm

### Release 0.23 (11. May 2017)

- bugfixes
	- cards glued to mouse after window was opened on some browsers
	- removed unnecessary java component (java.nio.file.CopyOption)

### Release 0.22 (16. April 2017)

- improvements
	- "go!" changed to "give me porn!"?
	- slightly more responsive design (stacks-view with flexible width)

### Release 0.21 (7. April 2017)

- hotfix
	- wrong release number
	- license as markdown

### Release 0.2 (2. April 2017)

- features
	- new project structure
  	- clear build pipeline (input > creator > output)
- improvements
	- design improvements
	- mouse icon changes depending on contest (mouseOver)
- fixes
	- bug: opening the creator makes all JS-files empty!
- a lot more!

### Release 0.1 (April 2016)

- first release
- primarily made for **Narrative Design Porn**
	- inspired by the previously created Level Design Porn (made with Wordpress)

## Credits (PornCreator)

- **csongor baranyai**  
	- csongorb (at) gmail (dot) com  
	- [www.csongorb.com](http://www.csongorb.com)

Thanks also for all the **universities / schools / institutions** where I had the possibility to lead a workshop and refine the materials. And of course a huge thank you to all the **students** who contributed through all their comments and critiques... and who tolerated my experiments.

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

All other media (images, software, etc.) remain the property of their copyright holders.
